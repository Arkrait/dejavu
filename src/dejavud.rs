use log::{info, trace, error, debug};
use crossbeam_channel::{bounded, Receiver};
use std::io;
use signal_hook::{SIGINT, SIGQUIT, SIGHUP, SIGTERM};
use std::os::raw::c_int;

use std::thread;
use signal_hook::iterator::Signals;
use serde::{Serialize, Deserialize};

use std::thread::JoinHandle;
use std::env::{var, set_var};
use tokio::timer::*;
use tokio::net::{TcpStream};
use tokio::prelude::*;
use tokio::io::read_to_end;
use std::io::{Read, BufReader};
use std::time::{Duration, Instant};

use notify_rust::Notification;

use std::os::unix::io::{FromRawFd, AsRawFd};
use std::os::unix::net;
use tokio::reactor::{Handle};

#[derive(Serialize, Deserialize, Debug)]
struct Message {
    title: String,
    millis_due: u64,
    body: String,
    icon: String
}

fn main() -> Result<(), io::Error>  {
    let log = var("RUST_LOG");
    if log.is_err() {
        // default value for logger
        set_var("RUST_LOG", "info");
    }
    env_logger::init();
    info!("Initiating dejavu daemon...");
    init();
    Ok(())
}

fn init() -> Result<(), io::Error> {
    trace!("Getting a signal receiver");
    let receiver = signals_channel()?;
    listen_socket();

    trace!("Spawning main loop thread");
    thread::spawn(move || {
        loop {
            let sig = receiver.recv().unwrap();
            info!("Got #{}", sig);
            match sig {
                SIGINT => {
                    trace!("Handling SIGINT...");
                    break;
                },
                SIGHUP => {
                    trace!("Handling SIGHUP...");
                    break;
                },
                SIGTERM => {
                    trace!("Handling SIGTERM...");
                    break;
                }
                _ => {
                    error!("Unknown signature!");
                    break;
                }
            }
        };
        shutdown()
    }).join();
    debug!("Closing app...");
    Ok(())
}

fn listen_socket() -> JoinHandle<()> {
    thread::spawn(|| {
        trace!("In thread {:?}", thread::current());
        debug!("Initiating listener...");
        let fd = std::fs::File::open("/tmp/dejavud.sock").unwrap().as_raw_fd();
        let _runtime: tokio::runtime::Runtime = tokio::runtime::Runtime::new().unwrap();

        let listener = unsafe {
            let listener = net::UnixListener::from_raw_fd(fd);
            tokio_uds::UnixListener::from_std(listener, &Handle::default())
        }.unwrap();

        let server = listener.incoming().for_each(move |sock| {
            trace!("Got connection {:?}", &sock);
            let sock = BufReader::new(sock);
            let buf = Vec::new();
            let handle_conn = read_to_end(sock, buf)
                .and_then(|(_r, b)| {
                    trace!("Read bytes: {:?}", &b);
                    let msg = parse_msg(b);
                    debug!("Starting wait on thread {:?}", thread::current());
                    let delay_fut = Delay::new(Instant::now() + Duration::from_millis(msg.millis_due))
                            .map_err(|e| {error!("{}", e)})
                            .and_then(|_| {
                                trace!("Waking up thread # {:?}", thread::current().id());
                                trace!("Spawning notification...");
                                spawn_notification(msg);
                                Ok(())
                            });
                    trace!("Spawning wait future...");
                    tokio::spawn(delay_fut);
                    Ok(())
                }).map_err(|e| {error!("{}", e)});
            tokio::spawn(handle_conn);
            Ok(())
        }).map_err(|e| {error!("{}", e)});

        tokio::run(server)
    })
}

fn parse_msg(msg: Vec<u8>) -> Message {
    let str = String::from_utf8(msg).unwrap();
    let m: Message = serde_json::from_str(&str).unwrap();
    trace!("Successfully parsed: {:?}", &m);
    m
}

fn spawn_notification(msg: Message) {
    Notification::new()
        .body(&msg.body)
        .summary(&msg.title)
        .icon(&msg.icon)
        .show();
}

fn shutdown() {
    info!("Shutting down!");
    std::fs::remove_file("/tmp/dejavud.sock");
}

fn reload() {

}

fn signals_channel() -> Result<Receiver<c_int>, io::Error> {
    let (sender, receiver) = bounded::<c_int>(100);
    let signals = Signals::new(&[SIGINT, SIGQUIT, SIGHUP])?;
    thread::spawn(move || {
        for signal in signals.forever() {
            let send_result = sender.send(signal);
            if send_result.is_err() {
                break;
            }
        }
    });
    Ok(receiver)
}